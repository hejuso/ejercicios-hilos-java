package ejemplos.hilos;

public class Ejercicio5Runnable implements Runnable {
	public void run() {

		for (int i = 1; i <= 10; i++) {
			try {
				System.out.println("Bienvenido, soy el " + Thread.currentThread().getName() + " y estoy en la iteraci��n " + i);
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Termina thread " + Thread.currentThread().getName());

	}

	public static void main(String[] args) throws InterruptedException {
		
		Thread hilo1 = new Thread(new Ejercicio5Runnable(), "hilo 1");
		Thread hilo2 = new Thread(new Ejercicio5Runnable(), "hilo 2");

		hilo1.start();
		hilo2.start();
		
		hilo1.join();
		hilo2.join();
		System.out.println("Termina thread main");

	}
}