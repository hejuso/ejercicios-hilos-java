package ejemplos.hilos;

public class Ejercicio5Thread extends Thread {
	public Ejercicio5Thread(String str) {
		super(str);
	}

	public void run() {
		for (int i = 1; i <= 10; i++) {
			try {
				System.out.println("Bienvenido, soy el "+getName() + " y estoy en la iteraci��n " + i);
				sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Termina thread " + getName());
	}

	public static void main(String[] args) throws InterruptedException {
		
		Ejercicio5Thread hilo1 = new Ejercicio5Thread("hilo 1");
		Ejercicio5Thread hilo2 = new Ejercicio5Thread("hilo 2");

		hilo1.start();
		hilo2.start();
		
		hilo1.join();
		hilo2.join();
		
		
		System.out.println("Termina thread main");
	}
}