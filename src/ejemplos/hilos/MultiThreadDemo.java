package ejemplos.hilos;

class NewThread implements Runnable {
	String name; // Se crea el una variable tipo String llamada name y una variable tipo Thread
					// llamada t
	Thread t;

	NewThread(String threadname) {
		name = threadname;
		t = new Thread(this, name);
		System.out.println("Nuevo hilo: " + t);
		t.start(); // Se crea un nuevo hilo al que le pasamos un string con el nombre del hilo y lo
					// ejecutamos
	}
	// Al ejecutar el hilo, se ejecutará la función run()

	public void run() {
		try {
			for (int i = 5; i > 0; i--) {
				System.out.println(name + ": " + i); // El for hace una cuenta atrás desde 5 y duerme el hilo un
														// segundo, cuando acabe la cuenta atrás, se mostrará el mensaje
														// diciendo que sale del hilo
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			System.out.println(name + "Interrupción del hilo hijo " + name);
		}

		System.out.println("Sale del hilo hijo " + name);
	}
}

class MultiThreadDemo {
	public static void main(String args[]) {
		new NewThread("Uno"); // Se crean los objetos y les pasamos la cadena de texto, que será el nombre del
								// thread
		new NewThread("Dos");
		new NewThread("Tres");
		try {
			// Se pone un tiempo de 10 segundos, que es lo que el hilo tardará en dormirse.
			Thread.sleep(10000);
		} catch (InterruptedException e) { //
			System.out.println("InterrupciÃ³n del hilo principal");
		}
		System.out.println("Sale del hilo principal.");
	}
}
