package ejemplos.hilos;

public class EjemploThread implements Runnable {

	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				System.out.println(Thread.currentThread().getName());
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) {

		new Thread(new EjemploThread(), "Hola").start();
		new Thread(new EjemploThread(), "Adios").start();
		System.out.println("Termina thread ");
	}
}
