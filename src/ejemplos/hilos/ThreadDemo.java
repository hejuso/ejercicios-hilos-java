package ejemplos.hilos;

class ThreadDemo implements Runnable {
	ThreadDemo() {
		Thread ct = Thread.currentThread(); // Se guarda en una variable tipo Thread el hilo actual y se muestra por
											// pantalla. Luego se hace lo mismo, pero con el hilo creado, al que se le
											// cambia el nombre del hilo por "demo Thread" y se ejecuta. El hilo se deja
											// inactivo durante 3 segundos y despu��s se sale del hilo actual.

		Thread t = new Thread(this);
		t.setName("demo Thread"); //
		System.out.println("hilo actual: " + ct);
		System.out.println("Hilo creado: " + t);
		t.start();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			System.out.println("Interrumpido");
		}
		System.out.println("saliendo del hilo main");
	}

	public void run() {
		try {
			for (int i = 5; i > 0; i--) { // Cuando se ejecuta el hilo, en un for se pone a dormir el hilo un segundo y,
											// como pasamos por el 5 veces en una cuenta atr��s hasta 1, estar�� un total
											// de 5 segundos. Se mostrar�� por pantalla el nombre del thread actual y la
											// posici��n del for.
				System.out.println(Thread.currentThread().getName() + " " + i);
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			System.out.println("hijo interrumpido");
		}
		System.out.println("saliendo del hilo hijo");
	}

	public static void main(String args[]) { // Una vez pasan los 5 segundos se mostrar�� un mensaje diciendo que se ha
												// salido del hilo hijo. En el main Crearemos el objeto ThreadDemo y lo
												// ejecutaremos.
		ThreadDemo hilo = new ThreadDemo();
		Thread h = new Thread(hilo);

	}
}
